#
# Gitlab CI/CD variables / functions for CI/CD scripts
#

# Save the path to the lib folder
lib_dir="$( cd "$( dirname "$( realpath "${BASH_SOURCE[0]}" )" )" && pwd )"

#
# GIT_SHA1
#   The SHA1 of the git repository HEAD or CI_COMMIT_SHA if it is set
#
export GIT_SHA1="${CI_COMMIT_SHA}"
if [ -z "${GIT_SHA1}" ]; then
	export GIT_SHA1="$(git rev-parse HEAD)"
fi

#
# load_registry_info()
#   Loads registry information from EC2 Parameter Store into environment variables
#
#   The following variables are set:
#	  AUTH_TYPE     - Value of CIBUILDER_REGISTRY_alias_AUTH_TYPE
#     ENDPOINT      - Value of CIBUILDER_REGISTRY_alias_ENDPOINT
#     REGION        - Value of CIBUILDER_REGISTRY_alias_REGION
#     REGISTRY_HOST - The host portion of the ENDPOINT variable
#                     If ENDPOINT is docker.io/<org>, this will be translated to https://index.docker.io/v1/.
#
#   Parameters:
#     alias - The registry alias to load
#
#   Returns:
#     0 if the settings were loaded, 1 if ENDPOINT is empty or 2 if AUTH_TYPE is not valid
#
function load_registry_info {
	local alias="$(echo $1 | awk '{ print toupper($0) }')"

	# Grab settings from EC2 parameter store
	AUTH_TYPE="$(eval "echo \$CIBUILDER_REGISTRY_${alias}_AUTH_TYPE")"
	ENDPOINT="$(eval "echo \$CIBUILDER_REGISTRY_${alias}_ENDPOINT")"
	REGION="$(eval "echo \$CIBUILDER_REGISTRY_${alias}_REGION")"
	
	# Check values
	if [ "${ENDPOINT}" == "" ]; then
		return 1
	fi
	case "${AUTH_TYPE}" in
		"docker"|"aws-ecr"|"none")
			;;
		"*")
			return 2
			;;
	esac

	# If the registry hostname is docker.io, we need to translate it for storing the login
	REGISTRY_HOST="$(echo $ENDPOINT | cut -d'/' -f1)"
	if [ "${REGISTRY_HOST}" == "docker.io" ]; then
		ENDPOINT="$(echo "${ENDPOINT}" | cut -d'/' -f2-)"
		REGISTRY_HOST="https://index.docker.io/v1/"
	fi

	# For AWS ECR, make sure we have a region set or infer it from the registry hostname
	if [ "${AUTH_TYPE}" == "aws-ecr" -a "${REGION}" == "" ]; then
		REGION="$(echo "${REGISTRY_HOST}" | cut -d'.' -f4)"
	fi
	return 0
}

#
# do_registry_login()
#   Performs authentication with the given Docker registry
#
#   Parameters:
#     alias - The registry alias to log into
#
#   Returns:
#     0 if the login was successful or 1 if not; exits with code 99 on error
#
function do_registry_login {
	local alias="$(echo $1 | awk '{ print toupper($0) }')"

	# Already authenticated
	if check_registry_login "${alias}"; then
		return 0
	fi

	echo -n "Logging into Docker registry '${alias}': "

	# Get credentials and authenticate
	local rc=0
	case $AUTH_TYPE in
		"docker")
			local cred="$(eval "echo \$CIBUILDER_REGISTRY_${alias}_CREDENTIALS" | awk '{ print toupper($0) }')"
			local username="$(eval "echo \$CIBUILDER_REGISTRY_CREDS_${cred}_USERNAME")"
			local password="$(eval "echo \$CIBUILDER_REGISTRY_CREDS_${cred}_PASSWORD")"
			result="$(docker login -u "${username}" -p "${password}" "${REGISTRY_HOST}" 2>&1)"
			if [ $? -ne 0 ]; then
				echo "[ FAILED ]"
				echo
				echo $result
				echo
				rc=1
			else
				echo "[ OK ]"
			fi
			;;

		"aws-ecr")
			cmd="$(${AWS_CMD} ecr get-login --no-include-email --region ${REGION} 2>&1)"
			rc=$?
			if [ $rc -ne 0 ]; then
				echo "[ FAILED ]"
				echo
				echo "$cmd (rc=$rc)"
				echo
			else
				result=$($cmd 2>&1)
				rc=$?
				if [ $rc -ne 0 ]; then
					echo "[ FAILED ]"
					echo
					echo "$result (rc=$rc)"
					echo
				else
					echo "[ OK ]"
				fi
			fi
			;;
		
		"none")
			echo "[ OK ]"
			;;

		*)
			echo "[ ERROR ]"
			echo
			echo "ERROR: Registry '${alias}' does not exist or does not have a valid 'authType' defined."
			echo
			exit 99
			;;
	esac
	return $rc
}
