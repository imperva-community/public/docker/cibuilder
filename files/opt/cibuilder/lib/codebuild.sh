#
# AWS CodeBuild variables / functions for CI/CD scripts
#

# Save the path to the lib folder
lib_dir="$( cd "$( dirname "$( realpath "${BASH_SOURCE[0]}" )" )" && pwd )"

#
# GIT_SHA1
#   The SHA1 of the git repository HEAD or CODEBUILD_SOURCE_VERSION if it is set
#
export GIT_SHA1="${CODEBUILD_SOURCE_VERSION}"
if [ -z "${GIT_SHA1}" ]; then
	export GIT_SHA1="$(git rev-parse HEAD)"
fi

#
# load_registry_info()
#   Loads registry information from EC2 Parameter Store into environment variables
#
#   The following variables are set:
#	  AUTH_TYPE     - Value of /CodeBuild/DockerRegistry/<alias>/authType
#     ENDPOINT      - Value of /CodeBuild/DockerRegistry/<alias>/endpoint
#     REGION        - Value of /CodeBuild/DockerRegistry/<alias>/region
#     REGISTRY_HOST - The host portion of the ENDPOINT variable
#                     If ENDPOINT is docker.io/<org>, this will be translated to https://index.docker.io/v1/.
#
#   Parameters:
#     alias - The registry alias to load
#
#   Returns:
#     0 if the settings were loaded, 1 if ENDPOINT is empty or 2 if AUTH_TYPE is not valid
#
function load_registry_info {
	local alias=$1

	# Grab settings from EC2 parameter store
	local param="$(${AWS_CMD} ssm get-parameters --names "/CodeBuild/DockerRegistry/${alias}/authType" \
		"/CodeBuild/DockerRegistry/${alias}/endpoint" "/CodeBuild/DockerRegistry/${alias}/region")"
	AUTH_TYPE="$(echo $param | jq -r ".Parameters | select(. != null)" | jq -r ".[] | select(.Name==\"/CodeBuild/DockerRegistry/${alias}/authType\") | .Value")"
	ENDPOINT="$(echo $param | jq -r ".Parameters | select(. != null)" | jq -r ".[] | select(.Name==\"/CodeBuild/DockerRegistry/${alias}/endpoint\") | .Value")"
	REGION="$(echo $param | jq -r ".Parameters | select(. != null)" | jq -r ".[] | select(.Name==\"/CodeBuild/DockerRegistry/${alias}/region\") | .Value")"

	# Check values
	if [ "${ENDPOINT}" == "" ]; then
		return 1
	fi
	case "${AUTH_TYPE}" in
		"docker"|"aws-ecr"|"none")
			;;
		"*")
			return 2
			;;
	esac

	# If the registry hostname is docker.io, we need to translate it for storing the login
	REGISTRY_HOST="$(echo $ENDPOINT | cut -d'/' -f1)"
	if [ "${REGISTRY_HOST}" == "docker.io" ]; then
		ENDPOINT="$(echo "${ENDPOINT}" | cut -d'/' -f2-)"
		REGISTRY_HOST="https://index.docker.io/v1/"
	fi

	# For AWS ECR, make sure we have a region set or infer it from the registry hostname
	if [ "${AUTH_TYPE}" == "aws-ecr" -a "${REGION}" == "" ]; then
		REGION="$(echo "${REGISTRY_HOST}" | cut -d'.' -f4)"
	fi
	return 0
}

#
# do_registry_login()
#   Performs authentication with the given Docker registry
#
#   Parameters:
#     alias - The registry alias to log into
#
#   Returns:
#     0 if the login was successful or 1 if not; exits with code 99 on error
#
function do_registry_login {
	local alias=$1

	# Already authenticated
	if check_registry_login "${alias}"; then
		return 0
	fi

	echo -n "Logging into Docker registry '${alias}': "

	# Get credentials and authenticate
	local rc=0
	case $AUTH_TYPE in
		"docker")
			local param="$(${AWS_CMD} ssm get-parameters --names "/CodeBuild/DockerRegistry/${alias}/authType" \
				"/CodeBuild/DockerRegistry/${alias}/endpoint" "/CodeBuild/DockerRegistry/${alias}/region")"
			local cred="$(echo $param | jq -r ".Parameters | select(. != null)" | jq -r ".[] | select(.Name==\"/CodeBuild/DockerRegistry/${alias}/credentials\") | .Value")"
			param="$(${AWS_CMD} ssm get-parameters --names "/CodeBuild/DockerRegistryCreds/${cred}/username" "/CodeBuild/DockerRegistryCreds/${cred}/password" --with-decryption)"
			local username="$(echo $param | jq -r ".Parameters | select(. != null)" | jq -r ".[] | select(.Name==\"/CodeBuild/DockerRegistryCreds/${cred}/username\") | .Value" | cut -d'/' -f1)"
			local password="$(echo $param | jq -r ".Parameters | select(. != null)" | jq -r ".[] | select(.Name==\"/CodeBuild/DockerRegistryCreds/${cred}/password\") | .Value" | cut -d'/' -f1)"
			result="$(docker login -u "${username}" -p "${password}" "${REGISTRY_HOST}" 2>&1)"
			if [ $? -ne 0 ]; then
				echo "[ FAILED ]"
				echo
				echo $result
				echo
				rc=1
			else
				echo "[ OK ]"
			fi
			;;

		"aws-ecr")
			cmd="$(${AWS_CMD} ecr get-login --no-include-email --region ${REGION} 2>&1)"
			rc=$?
			if [ $rc -ne 0 ]; then
				echo "[ FAILED ]"
				echo
				echo "$cmd (rc=$rc)"
				echo
			else
				result=$($cmd 2>&1)
				rc=$?
				if [ $rc -ne 0 ]; then
					echo "[ FAILED ]"
					echo
					echo "$result (rc=$rc)"
					echo
				else
					echo "[ OK ]"
				fi
			fi
			;;
		
		"none")
			echo "[ OK ]"
			;;

		*)
			echo "[ ERROR ]"
			echo
			echo "ERROR: Registry '${alias}' does not exist or does not have a valid 'authType' defined."
			echo
			exit 99
			;;
	esac
	return $rc
}
