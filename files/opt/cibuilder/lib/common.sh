#
# Common variables / functions for CI/CD scripts
#

# Turn debugging on
if [ "${CIBUILDER_ENABLE_DEBUG}" == "true" ]; then
	set -x
fi

# Save the path to the lib folder
lib_dir="$( cd "$( dirname "$( realpath "${BASH_SOURCE[0]}" )" )" && pwd )"

#------------------------------------------------------------------------------------------------------------
# CONFIG CHECK
#------------------------------------------------------------------------------------------------------------
settings_ok="$(jq . settings.json 2>&1)"
if [ $? -ne 0 ]; then
	echo
	echo "FATAL: 'settings.json' failed JSON validation"
	echo $settings_ok
	echo
	exit 99
fi

#------------------------------------------------------------------------------------------------------------
# GLOBAL VARIABLES
#------------------------------------------------------------------------------------------------------------

#
# AWS_COMMAND
#   Override the default 'aws' command
#
export AWS_CMD="${AWS_CMD:-aws}"

#
# IMAGE_NAME
#   The name of the Docker image
#
export IMAGE_NAME="$1"
if [ -z "${IMAGE_NAME}" ]; then
	echo
	echo "FATAL: You must specify the name of an image to build."
	echo
	exit 99
fi
settings="$(jq ".docker.images[\"${IMAGE_NAME}\"] | select(. != null)" settings.json)"
if [ -z "${settings}" ]; then
	echo
	echo "FATAL: The image '${IMAGE_NAME}' is not defined in the 'settings.json' file."
	echo
	exit 99
fi

#
# CI_TOOL
#   The CI/CD tool being used to do the build
#
export CI_TOOL="$(jq -r ".ci_tool | select(. != null)" settings.json)"
if [ -z "${CI_TOOL}" ]; then
	echo
	echo "FATAL: You must supply a 'ci_tool' value in the 'settings.json' file."
	echo
	exit 99
fi

# Suck in the CI tool's function library
if [ ! -f "${lib_dir}/${CI_TOOL}.sh" ]; then
	echo
	echo "FATAL: The CI tool '${CI_TOOL}' is not currently supported."
	echo
	exit 99
fi
. "${lib_dir}/${CI_TOOL}.sh"

#
# IMAGE_VERSION
#   The image version specified in the given Dockerfile
#
export IMAGE_VERSION="$(jq -r ".version | select(. != null)" settings.json)"
if [ -z "${IMAGE_VERSION}" ]; then
	echo
	echo "FATAL: You must supply a 'version' value in the 'settings.json' file."
	echo
	exit 99
fi

#
# GIT_SHORT_SHA1
#   The first 8 characters of the GIT_SHA1 variable
#
export GIT_SHORT_SHA1="${GIT_SHA1:0:8}"

#
# BUILD_TAG
#   The tag to use when building the container image
#
if [ -z "${GIT_SHORT_SHA1}" ]; then
	export BUILD_TAG="${IMAGE_VERSION}"
else
	export BUILD_TAG="${IMAGE_VERSION}-${GIT_SHORT_SHA1}"
fi

#------------------------------------------------------------------------------------------------------------
# FUNCTIONS
#------------------------------------------------------------------------------------------------------------

#
# check_registry_login()
#   Checks to see if the given registry has already been authenticated against so we don't have to perform auth
#   multiple times
#
#   Parameters:
#     alias - The registry alias to check
#
#   Returns:
#     0 if we already logged into the registry / login is not required or 1 if not; exits with code 99 on error
#
function check_registry_login {
	local alias=$1

	echo -n "Checking login for Docker registry '${alias}': "
	load_registry_info "${alias}"
	local rc=$?
	if [ $rc -ne 0 ]; then
		echo "[ ERROR ]"
		echo
		if [ $rc -eq 1 ]; then
			echo "ERROR: The registry '${alias}' is either not defined or does not have an endpoint set."
		elif [ $rc -eq 2 ]; then
			echo "ERROR: The registry '${alias}' is using an unsupported Docker authentication method."
		fi
		echo
		exit 99
	fi

	# If login is not required, just return now
	if [ "${AUTH_TYPE}" == "none" ]; then
		echo "[ LOGIN NOT REQUIRED ]"
		return 0
	fi

	# See if we have an the host stored already
	local auth=""
	if [ -e ~/.docker/config.json ]; then
		auth="$(jq -r ".auths[\"${REGISTRY_HOST}\"] | select(. != null)" ~/.docker/config.json)"
	fi

	# If we have not authenticated, return 1; otherwise return 0
	if [ -z "${auth}" ]; then
		echo "[ NOT LOGGED IN ]"
		return 1
	fi
	echo "[ LOGGED IN ]"
	return 0
}

#
# pull_image_from_registry()
#   Pulls an image from the registry
#
#   Parameters:
#     alias - The registry alias to pull the image from
#     image - The name of the image to pull from the registry
#
#   Returns:
#     returns 0 on success, 1 if the registry settings could not be loaded, 2 if the image could not
#     be tagged or 3 if a pull error occurred
#
function pull_image_from_registry {
	local alias=$1
	local image=$2

	# Load the registry settings
	echo -n "Loading '${alias}' registry settings: "
	load_registry_info "${alias}"
	local rc=$?
	if [ $rc -ne 0 ]; then
		echo "[ ERROR ]"
		echo
		if [ $rc -eq 1 ]; then
			echo "ERROR: The registry '${alias}' is either not defined or does not have an endpoint set."
		elif [ $rc -eq 2 ]; then
			echo "ERROR: The registry '${alias}' is using an unsupported Docker authentication method."
		fi
		echo
		return 1
	fi
	echo "[ OK ]"

	# Pull the image
	echo "Pulling image '${ENDPOINT}/${image}'..."
	docker pull "${ENDPOINT}/${image}"
	if [ $? -ne 0 ]; then
		echo
		echo "ERROR: Failed to pull image '${ENDPOINT}/${image}' from registry."
		echo
		return 3
	fi

	# Tag the image without its endpoint
	echo -n "Tagging image '${ENDPOINT}/${image}' as "${image}": "
	result="$(docker tag "${ENDPOINT}/${image}" "${image}" 2>&1)"
	if [ $? -ne 0 ]; then
		echo "[ ERROR ]"
		echo
		echo "ERROR: ${result}"
		echo
		return 2
	fi
	echo "[ OK ]"
	return 0
}

#
# push_image_to_registry()
#   Tags and pushes the image to the registry
#
#   Parameters:
#     alias - The registry alias to push the image for
#
#   Returns:
#     returns 0 on success, 1 if the registry settings could not be loaded, 2 if the image could not
#     be tagged or 3 if a push error occurred
#
function push_image_to_registry {
	local alias=$1
	shift
	local tags=( "$@" )

	# Load the registry settings
	echo -n "Loading '${alias}' registry settings: "
	load_registry_info "${alias}"
	local rc=$?
	if [ $rc -ne 0 ]; then
		echo "[ ERROR ]"
		echo
		if [ $rc -eq 1 ]; then
			echo "ERROR: The registry '${alias}' is either not defined or does not have an endpoint set."
		elif [ $rc -eq 2 ]; then
			echo "ERROR: The registry '${alias}' is using an unsupported Docker authentication method."
		fi
		echo
		return 1
	fi
	echo "[ OK ]"

	# Tag the image
	for tag in "${tags[@]}"; do
		echo -n "Tagging image '${IMAGE_NAME}:${BUILD_TAG}' as "${ENDPOINT}/${IMAGE_NAME}:${tag}": "
		result="$(docker tag "${IMAGE_NAME}:${BUILD_TAG}" "${ENDPOINT}/${IMAGE_NAME}:${tag}" 2>&1)"
		if [ $? -ne 0 ]; then
			echo "[ ERROR ]"
			echo
			echo "ERROR: ${result}"
			echo
			return 2
		fi
		echo "[ OK ]"
	done

	# Push the image and all tags together
	echo "Pushing image '${ENDPOINT}/${IMAGE_NAME}'..."
	docker push "${ENDPOINT}/${IMAGE_NAME}"
	if [ $? -ne 0 ]; then
		echo
		echo "ERROR: Failed to push image '${ENDPOINT}/${IMAGE_NAME}' to registry."
		echo
		return 3
	fi

	return 0
}
