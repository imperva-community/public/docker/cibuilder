#!/bin/bash
#
# Standard build script for building container images
#

#
# USAGE: build.sh <image name from settings.json>
#

script_dir="$( cd "$( dirname "$( readlink -f "${BASH_SOURCE[0]}" )" )" && pwd )"
$script_dir/prebuild-image.sh $1
rc=$?
if [ $rc -ne 0 ]; then
	echo "[FATAL] ${script_dir}/prebuild-image.sh returned non-zero error code $rc."
	exit $rc
fi
$script_dir/build-image.sh $1
rc=$?
if [ $rc -ne 0 ]; then
	echo "[FATAL] ${script_dir}/build-image.sh returned non-zero error code $rc."
	exit $rc
fi
$script_dir/postbuild-image.sh $1
rc=$?
if [ $? -ne 0 ]; then
	echo "[FATAL] ${script_dir}/postbuild-image.sh returned non-zero error code $rc."
	exit $rc
fi
exit 0
