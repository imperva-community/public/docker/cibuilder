#!/bin/bash
#
# Standard build script for building container images
#

#
# USAGE: postbuild-image.sh <image name from settings.json>
#

# Suck in common variables / functions
script_dir="$( cd "$( dirname "$( readlink -f "${BASH_SOURCE[0]}" )" )" && pwd )"
. $script_dir/../lib/common.sh $1

# Get settings from JSON file
mapfile -t registry_list < <( jq -r ".docker.images[\"${IMAGE_NAME}\"].push_to_registries | select(. != null)" settings.json | jq -r '.[]' )
only_extra_tags="$(jq -r ".docker.images[\"${IMAGE_NAME}\"].only_extra_tags | select(. != null)" settings.json)"
only_extra_tags="${only_extra_tags:-false}"
mapfile -t extra_tags < <( jq -r ".docker.images[\"${IMAGE_NAME}\"].extra_tags | select(. != null)" settings.json | jq -r '.[]' )
if [ "${only_extra_tags}" == "true" ]; then
    tags=( "${extra_tags[@]}" )
else
    tags=( "${BUILD_TAG}" "${IMAGE_VERSION}" "${extra_tags[@]}" )
fi

# Push image to each registry
for reg in "${registry_list[@]}"; do
	if ! push_image_to_registry "${reg}" "${tags[@]}"; then
		exit 99
	fi
done
