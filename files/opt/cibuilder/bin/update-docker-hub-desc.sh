
#username=
#password=
#org=
#repo=
#summary=
#desc=
#data_file=

token="$(curl -s -H "Content-Type: application/json" -X POST -d '{"username": "'${username}'", "password": "'${password}'"}' https://hub.docker.com/v2/users/login/ | jq -r .token)"
response="$(curl -sL -H "Content-Type: application/json" -H "Accept: application/json" -H "Authorization: JWT ${token}" -X PATCH -d "@${data_file}" "https://hub.docker.com/v2/repositories/${org}/${repo}/")"

# data_file
# {
#   "description": "${summary}",
#   "full_description": ${desc}"
# }
