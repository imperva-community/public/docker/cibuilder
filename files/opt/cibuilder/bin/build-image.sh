#!/bin/bash
#
# Standard build script for building container images
#

#
# USAGE: build-image.sh <image name from settings.json>
#

# Suck in common variables / functions
script_dir="$( cd "$( dirname "$( readlink -f "${BASH_SOURCE[0]}" )" )" && pwd )"
. $script_dir/../lib/common.sh $1

# Get settings from JSON file
dockerfile="$(jq -r ".docker.images[\"${IMAGE_NAME}\"].dockerfile | select(. != null)" settings.json)"
dockerfile="${dockerfile:-./Dockerfile}"
only_extra_tags="$(jq -r ".docker.images[\"${IMAGE_NAME}\"].only_extra_tags | select(. != null)" settings.json)"
only_extra_tags="${only_extra_tags:-false}"
mapfile -t extra_tags < <( jq -r ".docker.images[\"${IMAGE_NAME}\"].extra_tags | select(. != null)" settings.json | jq -r '.[]' )
if [ "${only_extra_tags}" == "true" ]; then
    tags="${extra_tags[@]}"
else
    tags="${BUILD_TAG} ${IMAGE_VERSION} ${extra_tags[@]}"
fi
mapfile -t build_opts < <( jq -r ".docker.images[\"${IMAGE_NAME}\"].build_options | select(. != null)" settings.json | jq -r '.[]' )

# Pull the image from cache, if enabled
pull_from_registry="$(jq -r ".docker.images[\"${IMAGE_NAME}\"].cache.pull_from_registry | select(. != null)" settings.json)"
image="$(jq -r ".docker.images[\"${IMAGE_NAME}\"].cache.image | select(. != null)" settings.json)"
if [ "${pull_from_registry}" != "" -a "${image}" != "" -a "${CIBUILDER_IGNORE_CACHE}" != "true" ]; then
	if ! pull_image_from_registry "${pull_from_registry}" "${image}"; then
        echo
		echo "WARNING: Unable to pull image from registry - disabling cache."
        echo
	else
        build_opts+=( "--cache-from ${image}" )
    fi
fi

# Build the image
echo "Building Docker image..."
docker build \
    --file "${dockerfile}" \
    --build-arg NAME="${IMAGE_NAME}" \
    --build-arg VERSION="${IMAGE_VERSION}" \
    --build-arg RELEASE_DATE="$(date +'%Y-%m-%d')" \
    --build-arg GIT_SHA1="${GIT_SHA1}" \
    --build-arg TAGS="${tags}" \
    --tag "${IMAGE_NAME}:${BUILD_TAG}" \
    ${build_opts[@]} .
