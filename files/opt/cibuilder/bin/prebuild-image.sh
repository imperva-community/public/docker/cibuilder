#!/bin/bash
#
# Standard pre-build script for building images
#

#
# USAGE: prebuild-image.sh <image name from settings.json>
#

# Suck in common variables / functions
script_dir="$( cd "$( dirname "$( readlink -f "${BASH_SOURCE[0]}" )" )" && pwd )"
. $script_dir/../lib/common.sh $1

# Make sure all file permissions are "standardized" in the image
if [ -d ./files ]; then
	echo "Resetting file permissions..."
	find ./files -type d -exec chmod 0755 {} \;
	if [ $? -ne 0 ]; then
		exit 99
	fi
	find ./files -type f -exec chmod 0644 {} \;
	if [ $? -ne 0 ]; then
		exit 99
	fi
fi

# Log into each registry we are pushing to
mapfile -t registry_list < <( jq -r ".docker.images[\"${IMAGE_NAME}\"].push_to_registries | select(. != null)" settings.json | jq -r '.[]' )
for reg in "${registry_list[@]}"; do
	if ! do_registry_login "${reg}"; then
		exit 99
	fi
done

# Log into the registry we are pulling the cache from, if enabled
pull_from_registry="$(jq -r ".docker.images[\"${IMAGE_NAME}\"].cache.pull_from_registry | select(. != null)" settings.json)"
if [ "${pull_from_registry}" != "" -a "${CIBUILDER_IGNORE_CACHE}" != "true" ]; then
	if ! do_registry_login "${reg}"; then
		exit 99
	fi
fi
