# Configure prompt
NORMAL="\[\e[0m\]"
RED="\[\e[1;31m\]"
GREEN="\[\e[1;32m\]"
CYAN="\[\e[1;36m\]"
BLUE="\[\e[1;34m\]"
YELLOW="\[\e[1;33m\]"
PURPLE="\[\e[1;35m\]"
LT_BLUE="\[\e[1;94m\]"
img="${IMAGE_NAME}"
if [ ! -z "${CONTAINER_NAME}" ]; then
	img="${CONTAINER_NAME}::${IMAGE_NAME}"
fi
if [ -z "$USER" -o "$USER" == "root" ]; then
	PS1="${NORMAL}[${YELLOW}${img}${NORMAL}] ${RED}\u${NORMAL}@${PURPLE}\h${NORMAL}:${CYAN}\w${RED} #${NORMAL} "
else
	PS1="${NORMAL}[${YELLOW}${img}${NORMAL}] ${GREEN}\u${NORMAL}@${PURPLE}\h${NORMAL}:${CYAN}\w${GREEN} \$${NORMAL} "
fi
