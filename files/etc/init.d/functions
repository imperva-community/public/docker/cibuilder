#
# Shared service functions
#

#
# getpid(daemon)
#   Outputs the PID of the given daemon if it is running or nothing if it is not
#
getpid() {
	daemon=$1

	pgrep -f "${daemon}"
	return 0
}

#
# start(name, daemon, args)
#   Starts the daemon service displaying the given name
#
start() {
	name=$1
	shift
	daemon=$1
	shift
	args="$@"

	printf "%-40s " "Starting service '${name}': "
	pid=$(getpid "${daemon}")
	if [ "${pid}" == "" ]; then
		${daemon} "${args[@]}"
		if [ $? -ne 0 ]; then
			failed
		else
			success
		fi
	else
		success
	fi
}

#
# stop(name, daemon)
#   Stops the daemon service displaying the given name
#
stop() {
	name=$1
	daemon=$2

	printf "%-40s " "Stopping service '${name}': "
	pid=$(getpid "${daemon}")
	if [ "${pid}" != "" ]; then
		kill -TERM $pid
		if [ $? -ne 0 ]; then
			failed
		else
			success
		fi
	else
			success
	fi
}

#
# status(name, daemon)
#   Shows status for the daemon service displaying the given name
#
status() {
	name=$1
	daemon=$2

	pid=$(getpid "${daemon}")
	if [ "${pid}" != "" ]; then
		echo "'${name}' service is running (pid: ${pid})."
	else
		echo "'${name}' service is not running."
	fi
}

#
# success()
#   Prints [ OK ] message
#
success() {
	green='\033[1;32m'
	normal='\033[0m'
	printf "${normal}[ ${green}OK${normal} ]\n"
}

#
# failed()
#   Prints [ FAILED ] message
#
failed() {
	red='\033[1;31m'
	normal='\033[0m'
	printf "${normal}[ ${red}FAILED${normal} ]\n"
}

#
# skipped()
#   Prints [ SKIPPED ] message
#
skipped() {
	blue='\033[1;34m'
	normal='\033[0m'
	printf "${normal}[ ${blue}SKIPPED${normal} ]\n"
}
