# CI/CD Builder Image

[![pipeline status](https://gitlab.com/imperva-community/public/docker/cibuilder/badges/master/build.svg)](https://gitlab.com/imperva-community/public/docker/cibuilder/pipelines)
[![license](https://img.shields.io/badge/license-imperva--community-blue.svg)](https://gitlab.com/imperva-community/public/docker/cibuilder/blob/master/LICENSE.md)
[![support](https://img.shields.io/badge/support-community-blue.svg)](https://gitlab.com/imperva-community)

## Description

This image is designed as an environment for building Docker container images as well as any code written in tools such as Go, Node.js, Python, etc. using a list of supported CI/CD tools.  The image is based off of the Docker in Docker stable image from the Docker Hub repository running on **Alpine Linux**.

The currently supported list of CI/CD tools includes:

- Gitlab (Community Edition, Enterprise Edition and Cloud)
- AWS CodeBuild

If demand warrants it, additional tools may be added in the future.

## Included Tools

The following packages are included in this Alpine Linux-based image:

- bash
- build-base
- ca-certificates
- cifs-utils
- coreutils
- curl
- curl-dev
- file
- gcc
- git
- glide
- go@edge
- jq
- libcap
- linux-headers
- musl-dev
- nfs-utils
- nodejs
- nodejs-npm
- openssl
- openssl-dev
- py3-pip
- py-pip
- python
- python3
- python-dev
- su-exec
- syslog-ng
- syslog-ng-json
- tzdata
- unzip
- xz

The following additional Python 2 packages are installed:

- awscli
- certifi
- docker-compose
- netifaces
- pycurl

## Startup Process

When this image starts, it launches `/usr/local/sbin/dumb-init` as a minimal init system (see links at the bottom for details on **dumb-init**).  The init system will run the `/init.sh` script, which is in charge of setting up the environment and starting an extra services required by the container before finally starting a Bash login shell by default.

Here's a short summary of what the `/init.sh` script does:

- `/usr/local/sbin/dumb-init` launches the `/init.sh` script in the image.
- `/init.sh` sets the time zone for the container (see **Setting the Time Zone** below).
- `/init.sh` adds an entry for **dockerhost** to `/etc/hosts` which points to the IP address on the container's network of the host on which the container is running
- `/init.sh` launches any services which should be started up at boot (see **Services** section below).
- `/init.sh` then replaces itself by launching the command provided by **CMD** in the Dockerfile.

## Setting the Time Zone

A container's timezone can be changed from the default UTC timezone to a local timezone by setting the **TZ** environment variable to a standard Linux timezone (eg: **America/New_York**, **PDT**, etc.).  If the variable is not defined, the timezone is automatically set to UTC.

## Services

Service configuration allows you to automatically start additional services when the container is booted from the `/init.sh` startup script.  By default, the services specified by the **ENABLE_SERVICES** environment variable will be started in the order specified when the container is started.  If the variable is empty, no services will be started automatically.

The services included in this image are:

- **automount** for automatically mounting external filesystems such as CIFS and NFS
- **crond** for running jobs periodically
- **docker** for starting Docker services, which may be required by your CI/CD provider if you are building a Docker image inside this image
- **syslog-ng** for sending syslog messages to an external location

You can include additional service scripts by mounting a local volume under `/etc/init.d/services.d` and placing service startup scripts in that folder.

Service scripts can be placed in either `/etc/init.d/services` or `/etc/init.d/services.d`.  The former is intended for use when building container images and adding services.  They are considered *system* services.  The latter is intended to allow for additional startup scripts to be run for an individual container.  The folder should be a volume that is mapped back to the container host.

You can use the `service <service> [action]` command to run actions on services or use `service --list` to list out all services available.  Each service can have its own list of valid actions, however, every service _must_ accept `start` as an action but _should_ accept `stop`, `restart` and `status` actions as well.  It is recommended that you follow the same basic pattern as the `/etc/init.d/services/crond` startup script built into the image.

Finally, you can abort container startup if a service fails to start (by returning a non-zero exit code from its startup script) by setting the **EXIT_ON_SERVICE_FAILURE** variable to `true`.

## Mounting NFS/CIFS File Systems at Startup

This container contains NFS and CIFS utilities for mounting remote file systems. To mount additional file shares, you should place configuration files (see below) in the **/etc/automount/conf.d** folder and you must enable the **automount** service in the container through the use of the **IMAGE_SERVICES** or **CONTAINER_SERVICES** variables.

The following example mounts a CIFS share using a credentials file stored in the **/etc/automount/creds.d** folder:

```mount.d
TYPE=cifs
REMOTE_PATH=//windows_server.domain/share_name/path
MOUNT_POINT=/mnt/share_name
OPTIONS=credentials=/etc/automount/creds.d/share_credentials,rw
```

For information on CIFS credentials files, please review the man page for **mount.cifs**.

The following example mounts an NFS share:

```mount.d
TYPE=nfs
REMOTE_PATH=192.168.1.1:/path/to/folder
MOUNT_POINT=/mnt/share_name
OPTIONS=rw
```

**NOTE:** You must start the container in privileged mode if you wish to mount file systems.  For details on how to run the container in privileged mode, please review your CI/CD provider's documentation.

## syslog-ng Integration

**syslog-ng** and **syslog-ng-json** are included in this image in order to provide robust logging capabilities out of the box.

Configuration files for **syslog-ng** should be stored in `/etc/syslog-ng/conf.d`, which should be a volume mapped back to the container host.

By default syslog-ng is not configured to log anything anywhere.  The **syslog-ng-json** module is so that you can log JSON-based messages as well.

## Building Docker Images

This image includes scripts to assist you in building Docker images for your project.  If you choose to build Docker images with this image, you may or may not need to start the **docker** service when the container starts.  You'll need to review your CI/CD provider's documentation for details on how they implement **Docker in Docker** in their environment.

You can override the arguments passed to `/usr/local/bin/dockerd` when starting the **docker** service by setting the **DOCKERD_ARGS** variable value to any valid arguments for `dockerd`.  By default **DOCKERD_ARGS** is set to `--host=unix:///var/run/docker.sock --host=tcp://127.0.0.1:2375 --storage-driver=overlay`.

The scripts to assist you in building Docker images are outlined in the sections below.  Each of these scripts relies on a **settings.json** file, which must be placed at the root of your repository in order for them to work.  The format and settings for this file are described in the next section.

### settings.json Configuration

The **settings.json** file in the root of your repository must follow standard JSON guidelines and will need to include the configuration settings described below.  To better understand the format and settings in the file, we'll start with the following sample configuration:

```settings.json
{
  "ci_tool": "gitlab",
  "version": "1.0.0",
  "docker": {
    "images": {
      "alpine": {
        "cache": {
          "pull_from_registry": "DockerHub",
          "image": "alpine:latest"
        },
        "dockerfile": "./Dockerfile",
        "build_options": [
          "--add-host myhost=1.2.3.4",
          "--rm"
        ],
        "extra_tags": [
          "latest",
          "ga",
          "1.0"
        ],
        "only_extra_tags": "false",
        "push_to_registries": [
          "ECR-East",
          "DockerHub"
        ]
      }
    }
  }
}
```

- **ci_tool** _(Added in 1.5.0)_: the CI/CD tool being used to build the container image; The following CI/CD tools are currently supported: **gitlab** for GitLab CI/CD and **codebuild** for AWS CodeBuild. **Required**

- **version**: the version that will be used for the image; This is used for tagging the image automatically. **Required**

- **docker**: the Docker settings for building the image; It must contain a single object called **images** whose subkeys indicate the value of the **IMAGE_NAME** variable that will be used when building the image. **Required**

- **cache** _(Added in 1.5.0)_: if defined, pulls an existing image from the given registry and uses it as a cache; If caching is enabled and you would like to disable it only during a specific build, set the environment variable **CIBUILDER_IGNORE_CACHE** to `true` for the build. If either **pull_from_registry** or **image** are missing under this key, caching will be disable. _Default: no caching enabled_

- **pull_from_registry** _(Added in 1.5.0)_: the Docker registry alias to use to pull the image locally **Required** _(if **cache** defined)_

- **image** _(Added in 1.5.0)_: the image name and tag to pull from the endpoint associated with the registry alias; Note this should typically be the latest tagged version of same image you are building but you can use any image and tag here so long as it exists in the registry. **Required** _(if **cache** defined)_

- **dockerfile**: the relative path to the Dockerfile for the image; The path is relative to the root folder of the repository. _Default: **./Dockerfile**_

- **build_options**: a list of any extra options you wish to pass to the `docker build` command _Default: no additional options_

- **extra_tags**: a list of tags to automatically push to the Docker registry once the image is built. _Default: no additional tags_

- **only_extra_tags** _(Added in 1.5.0)_: if set to `true`, only push tags specified by **extra_tags** to the given registries; By default, a build tag containing the version and first portion of the SHA1 of the latest git commit and the version by itself are pushed to the Docker registry. _Default: **false**_

- **push_to_registries**: a list of alias names of Docker registries to which you wish to push the image; These are defined in your CI/CD provider as described in the next sections. _Default: no registries_

**Note:** While it's generally not recommended or even _allowed_ to use spaces in many of these settings, if you have any values that have a space in them, be sure to surround the value with backslash double-quote (`\"`) characters so that the spacing will be preserved when passed to the various scripts and commands used during the image build process.

### Configuring Gitlab CI/CD

If your image requires no special configurations, the Gitlab CI/CD **.gitlab-ci.yml** file can simply call the build scripts in the image to automatically perform the build for you. You can either use the full **build.sh** script to build everything in one step or use the individual scripts to break the build into prebuild, build, and postbuild steps.

A basic configuration might look like this:

```yaml
image: registry.gitlab.com/imperva-community/public/docker/cibuilder:stable

services:
- docker:dind

variables:
  # When using dind service we need to instruct docker, to talk with the
  # daemon started inside of the service. The daemon is available with
  # a network connection instead of the default /var/run/docker.sock socket.
  #
  # The 'docker' hostname is the alias of the service container as described at
  # https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services
  #
  # Note that if you're using Kubernetes executor, the variable should be set to
  # tcp://localhost:2375 because of how Kubernetes executor connects services
  # to the job container
  DOCKER_HOST: tcp://docker:2375/
  # When using dind, it's wise to use the overlayfs driver for
  # improved performance.
  DOCKER_DRIVER: overlay2

stages:
- build

build:
  stage: build
  script:
    - build.sh "alpine"
```

**Note:** By using the Gitlab Docker-in-Docker service, you do not need to run Docker locally within the build container, but you must specify the **DOCKER_HOST** variable so Docker knows which host to connect to in order to perform commands.  You do not necessarily need to specify **DOCKER_HOST** and **DOCKER_DRIVER** directly in your repository's YAML configuration file.  These values could also be stored in project or group variables as well.

#### CI/CD Variables

Registry aliases are used in the **settings.json** file to indicate which Docker registry or registries an image should be pushed to.  The aliases correspond to settings within variables exposed through Gitlab's CI/CD tool.  Any supported method of exposing variables in Gitlab is supported, but you may wish to limit access to any groups or projects which contain sensitive credentials.

Each registry alias must correspond to a variable whose name starts with **CIBUILDER_REGISTRY_** and is followed by the capitalized version of the alias, an underscore and the variable name.  For example, the variables for the registry alias **DockerHub** would all start with **CIBUILDER_REGISTRY_DOCKERHUB_**.

Each registry alias must define the following values:

- **AUTH_TYPE**: indicates the type of registry authentication that's required; use **none** if no authentication is required, **docker** for a standard Docker registry or Docker Hub or **aws-ecr** for AWS ECS Container Registry
- **ENDPOINT**: indicates the full path to the Docker registry endpoint; Docker Hub endpoints should start with **docker.io/** followed by the organization or user name.  AWS ECR endpoints are simply the value of **Repository URI** up to the first **/** character.  Other Docker registry endpoints typically are the hostname of the registry followed by any optional path parameters.  Gitlab, for example, has a built-in **CI_PROJECT_NAMESPACE** variable that can be used in the **ENDPOINT** value by specifying **registry.gitlab.com/${CI_PROJECT_NAMESPACE}** as the **ENDPOINT**.
- **REGION** _(AWS ECR only)_: the region in which the container registry resides; Typically this is not necessary unless the ECR endpoint does not follow the normal **[account_id].dkr.ecr.[region].amazonaws.com** naming convention.  IF the ECR endpoint does not follow the normal convention then you must define this parameter.

For registries with **AUTH_TYPE** set to **docker**, a **CREDENTIALS** value must be set which corresponds to variables whose names begin with **CIBUILDER_REGISTRY_CREDS_** followed by the capitalized value of the **CREDENTIALS** variable, an underscore and the variable name. There must be 2 variables defined:

- **USERNAME**: the username to use when logging into the registry
- **PASSWORD**: the password for the user

Here's an example of variables that would need to be defined for a self-hosted registry located at **https://registry.mydomain.com/myorg** aliased as **MyDocker** which requires no authentication:

```text
CIBUILDER_REGISTRY_MYDOCKER_AUTH_TYPE=none
CIBUILDER_REGISTRY_MYDOCKER_ENDPOINT=registry.mydomain.com/myorg
```

Here's an example of variables that would need to be defined for the Docker Hub registry aliased as **DockerHub**:

```text
CIBUILDER_REGISTRY_DOCKERHUB_AUTH_TYPE=docker
CIBUILDER_REGISTRY_DOCKERHUB_ENDPOINT=docker.io/impervainc
CIBUILDER_REGISTRY_DOCKERHUB_CREDENTIALS=DockerHubImperva
CIBUILDER_REGISTRY_CREDS_DOCKERHUBIMPERVA_USERNAME=myuser
CIBUILDER_REGISTRY_CREDS_DOCKERHUBIMPERVA_PASSWORD=mypassword
```

Here's an example of parameters that would need to be defined for the AWS ECR registry aliased as **ECR-East**:

```text
CIBUILDER_REGISTRY_ECREAST_AUTH_TYPE=aws-ecr
CIBUILDER_REGISTRY_ECREAST_ENDPOINT=123456789012.dkr.ecr.us-east-2.amazonaws.com
CIBUILDER_REGISTRY_ECREAST_REGION=us-east-2
```

Note that the **region** parameter is optional in this case because the endpoint follows the standard AWS ECR endpoint naming convention with the region in the 4th part of the name.

Finally, here's an example for Gitlab Cloud aliased as **GitlabCloud** which can be used for any Gitlab Cloud hosted code repository using Gitlab's **CI_PROJECT_NAMESPACE** variable and automatically provided CI token using **CI_JOB_TOKEN**:

```text
CIBUILDER_REGISTRY_GITLABCLOUD_AUTH_TYPE=docker
CIBUILDER_REGISTRY_GITLABCLOUD_ENDPOINT=registry.gitlab.com/${CI_PROJECT_NAMESPACE}
CIBUILDER_REGISTRY_GITLABCLOUD_CREDENTIALS=GitlabCloud
CIBUILDER_REGISTRY_CREDS_GITLABCLOUD_USERNAME=gitlab-ci-token
CIBUILDER_REGISTRY_CREDS_GITLABCLOUD_PASSWORD=$CI_JOB_TOKEN
```

### Configuring AWS CodeBuild

If your image requires no special configurations, the AWS CodeBuidl **buildspec.yml** file can simply call the build scripts in the image to automatically perform the build for you. You can either use the full **build.sh** script to build everything in one step or use the individual scripts to break the build into prebuild, build, and postbuild steps.

A basic configuration might look like this:

```yaml
version: 0.2

phases:
  build:
    commands:
      - build.sh "alpine"
```

while a split configuration might look like this:

```yaml
version: 0.2

phases:
    pre_build:
        commands:
            - prebuild-image.sh "alpine"

    build:
        commands:
            - build-image.sh "alpine"

    post_build:
        commands:
            - postbuild-image.sh "alpine"
```

#### EC2 Parameter Store Settings

Registry aliases are used in the **settings.json** file to indicate which Docker registry or registries an image should be pushed to.  The aliases correspond to settings within the EC2 Parameter Store.  Be sure that the IAM user you are using with your build has permissions to access the parameters in the store.

Each registry alias must correspond to a parameter whose name starts with **/CodeBuild/DockerRegistry/** and is followed by the alias.  For example, the settings for the registry alias **DockerHub** would all start with **/CodeBuild/DockerRegistry/DockerHub/**.

Each registry alias must define the following value parameters:

- **authType** (String): indicates the type of registry authentication that's required; use **none** if no authentication is required, **docker** for a standard Docker registry or Docker Hub or **aws-ecr** for AWS ECS Container Registry
- **endpoint** (String): indicates the full path to the Docker registry endpoint; Docker Hub endpoints should start with **docker.io/** followed by the organization or user name.  AWS ECR endpoints are simply the value of **Repository URI** up to the first **/** character.  Other Docker registry endpoints typically are the hostname of the registry followed by any optional path parameters.
- **region** (String) _(AWS ECR only)_: the region in which the container registry resides; Typically this is not necessary unless the ECR endpoint does not follow the normal **[account_id].dkr.ecr.[region].amazonaws.com** naming convention.  IF the ECR endpoint does not follow the normal convention then you must define this parameter.

For registries with **authType** set to **docker**, a **credentials** parameter must be set which corresponds to parameters whose names begin with **/CodeBuild/DockerRegistryCreds/** followed by the value of the **credentials** parameter. There must be 2 parameters defined:

- **username** (SecureString): the username to use when logging into the registry
- **password** (SecureString): the password for the user

Here's an example of parameters that would need to be defined for a self-hosted registry located at **https://registry.mydomain.com/myorg** aliased as **MyDocker** which requires no authentication:

```text
Name:  /CodeBuild/DockerRegistry/MyDocker/authType
Type:  String
Value: none

Name:  /CodeBuild/DockerRegistry/MyDocker/endpoint
Type:  String
Value: registry.mydomain.com/myorg
```

Here's an example of parameters that would need to be defined for the Docker Hub registry aliased as **DockerHub**:

```text
Name:  /CodeBuild/DockerRegistry/DockerHub/authType
Type:  String
Value: docker

Name:  /CodeBuild/DockerRegistry/DockerHub/endpoint
Type:  String
Value: docker.io/impervainc

Name:  /CodeBuild/DockerRegistry/DockerHub/credentials
Type:  String
Value: DockerHubImperva

Name:  /CodeBuild/DockerRegistryCreds/DockerHubImperva/username
Type:  SecureString
Value: myuser

Name:  /CodeBuild/DockerRegistryCreds/DockerHubImperva/password
Type:  SecureString
Value: mypass
```

Here's an example of parameters that would need to be defined for the AWS ECR registry aliased as **ECR-East**:

```text
Name:  /CodeBuild/DockerRegistry/ECR-East/authType
Type:  String
Value: aws-ecr

Name:  /CodeBuild/DockerRegistry/ECR-East/endpoint
Type:  String
Value: 123456789012.dkr.ecr.us-east-2.amazonaws.com

Name:  /CodeBuild/DockerRegistry/ECR-East/region
Type:  String
Value: us-east-2
```

Note that the **region** parameter is optional in this case because the endpoint follows the standard AWS ECR endpoint naming convention with the region in the 4th part of the name.

### Build Scripts

This container image contains scripts to help automatically build and configure Docker containers.  The scripts live in the `/opt/cibuilder/bin` folder, which is part of the system path, and utilize the **settings.json** file, which should be placed in the root folder of the repository.

During any of the build scripts below, you can override the following commands by setting the appropriate environment variable in your CI/CD tool or configuration file:

**AWS_CMD**: Overrides the `aws` command; _Example: use `aws --profile=my_profile` if you wish to use the **my_profile** profile from your AWS credentials file._

The following scripts can be called from your CI/CD configuration file:

**build.sh** simply calls **prebuild-image.sh** followed by **build-image.sh** and finally **postbuild-image.sh**.  It is a wrapper to simplify configuration if you are just building a standard image.  The script takes a single argument, the name of the Docker image from **settings.json** that is being built.

**prebuild-image.sh** will automatically reset file permissions on everything in the repository under the **files** subdirectory back to 0644 for files and 0755 for directories.  It will then automatically log into any Docker registries to which the container is going to be pushed.  The script takes a single argument, the name of the Docker image from **settings.json** that is being built.

**build-image.sh** simply builds the Docker image using the specified Dockerfile that is defined in the **settings.json** file.  The script takes a single argument, the name of the Docker image from **settings.json** that is being built.  It automatically passes the following build arguments for use in the Dockerfile, if desired:

- **NAME**: name of the image being built
- **VERSION**: version from the **settings.json** file
- **RELEASE_DATE**: the current date in YYYY-mm-dd format
- **GIT_SHA1**: the SHA1 of the latest git commit
- **TAGS**: any tags specified for the Docker image from the **settings.json** file plus the default tags, if they have not been disabled

Below is an excerpt from a Dockerfile showing how you may choose to use these variables to add metadata to the image.

```Dockerfile
# Build arguments
ARG NAME
ARG VERSION
ARG RELEASE_DATE
ARG GIT_SHA1
ARG TAGS

# Image build metadata
ENV IMAGE_NAME "${NAME}"
ENV IMAGE_VERSION "${VERSION}"
ENV IMAGE_RELEASE_DATE "${RELEASE_DATE}"
LABEL \
  vendor="Imperva, Inc." \
  maintainer="Imperva GSA Team <gsa-team@imperva.com>" \
  com.imperva.image_name="${IMAGE_NAME}" \
  com.imperva.image_version="${IMAGE_VERSION}" \
  com.imperva.image_release_date="${IMAGE_RELEASE_DATE}" \
  com.imperva.image_tags="${TAGS}" \
  com.imperva.commit_id="${GIT_SHA1}"
```

**postbuild-image.sh** handles tagging the newly created image and then pushing it to the given Docker registries.
Unless disabled in **settings.json**, every image is automatically tagged with the **BUILD_TAG** and **VERSION** when pushed to the Docker registry.  It is also tagged with any **EXTRA_TAGS** that are specified.

### Debugging Mode

If you'd like to enable _debug_ mode in order to view all statements that are executed during the build, simply set the **CIBUILDER_ENABLE_DEBUG** variable to the string `true` in your CI/CD build environment.

### Deprecated Scripts

The build scripts have been relocated from **/usr/local/codebuild** and renamed starting in version 1.5.0.  The path to the old scripts should continue to work (as symbolic links) until version 2.0.0 is released.  Once version 2.0.0 has been released, the symbolic links to the old paths will be removed.

```text
/usr/local/codebuild/prebuild-container.sh --> /opt/cibuilder/bin/prebuild-image.sh
/usr/local/codebuild/build-container.sh --> /opt/cibuilder/bin/build-image.sh
/usr/local/codebuild/postbuild-container.sh --> /opt/cibuilder/bin/postbuild-image.sh
```

## Additional Help or Questions

If you have questions, find bugs or need additional help, please send an email to:
[gsa-team@imperva.com](mailto:gsa-team@imperva.com).

## Links

- [Docker in Docker Image](https://hub.docker.com/_/docker/)
- [Yelp dumb-init](https://github.com/Yelp/dumb-init)
- [syslog-ng](https://syslog-ng.org)
- [AWS CodeBuild](https://aws.amazon.com/codebuild/)
- [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/README.html)
