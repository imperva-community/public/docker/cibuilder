#
# Dockerfile for CI/CD builder image
#
FROM docker:stable-dind

# dumb-init info
ARG DUMB_INIT_VERSION=1.2.1
ARG DUMB_INIT_URL=https://github.com/Yelp/dumb-init/releases/download/v${DUMB_INIT_VERSION}

# Install packages
#   Add the edge repo and do any package upgrades
#   Add core packages and syslog packages
#   Add local utilities
#   Cleanup
RUN set -xe \
	&& echo @edge http://dl-cdn.alpinelinux.org/alpine/edge/main | tee -a /etc/apk/repositories \
	&& apk update \
	&& apk upgrade \
	&& apk add --update curl bash file su-exec ca-certificates coreutils libcap tzdata xz openssl jq unzip \
		nfs-utils cifs-utils build-base python py-pip python3 py3-pip go@edge nodejs nodejs-npm git \
	    gcc curl-dev python-dev musl-dev openssl-dev linux-headers glide syslog-ng@edge syslog-ng-json@edge \
	&& pip install --upgrade --no-cache-dir pip certifi pycurl netifaces docker-compose awscli \
	&& pip3 install --upgrade pip \
	&& mkdir -p /usr/local/bin /usr/local/sbin \
	&& curl -L ${DUMB_INIT_URL}/dumb-init_${DUMB_INIT_VERSION}_amd64 -o /usr/local/sbin/dumb-init \
	&& rm -rf /var/cache/apk/* /tmp/* /var/tmp/* /etc/init.d/*

# Add files to image
COPY files /

# Configure image
RUN set -xe \
	&& chmod +x /init.sh /usr/local/bin/* /usr/local/sbin/* /etc/init.d/services/* /opt/cibuilder/bin/* \
	&& chmod 0700 /root \
	&& rm -f /etc/profile.d/color_prompt \
	&& update-ca-certificates \
	&& sed -i -e 's|/bin/ash|/bin/bash|g' /etc/passwd \
	&& mkdir -p /var/spool/syslog-ng \
	&& chgrp -R adm /var/spool/syslog-ng

# Add symlinks for deprecated script paths
# TODO: Remove in v2.0.0
RUN set -xe \
	&& ln -sf /opt/cibuilder/bin /usr/local/codebuild \
	&& ln -sf prebuild-image.sh /opt/cibuilder/prebuild-container.sh \
	&& ln -sf build-image.sh /opt/cibuilder/build-container.sh \
	&& ln -sf postbuild-image.sh /opt/cibuilder/postbuild-container.sh

# Export volumes
VOLUME [ "/etc/init.d/services.d", "/etc/syslog-ng/conf.d", "/etc/automount/conf.d", "/etc/automount/creds.d" ]

# Set entry point
ENTRYPOINT [ "/usr/local/sbin/dumb-init", "--", "/init.sh" ]

# Configure default command
CMD [ "/bin/bash", "-l" ]

# Required build arguments
ARG NAME
ARG VERSION
ARG RELEASE_DATE
ARG GIT_SHA1
ARG TAGS

# Image build metadata
ENV IMAGE_NAME "${NAME}"
ENV IMAGE_VERSION "${VERSION}"
ENV IMAGE_RELEASE_DATE "${RELEASE_DATE}"
LABEL \
	vendor="Imperva, Inc." \
	maintainer="Imperva GSA Team <gsa-team@imperva.com>" \
	com.imperva.image_name="${IMAGE_NAME}" \
	com.imperva.image_version="${IMAGE_VERSION}" \
	com.imperva.image_release_date="${IMAGE_RELEASE_DATE}" \
	com.imperva.image_tags="${TAGS}" \
	com.imperva.commit_id="${GIT_SHA1}"
