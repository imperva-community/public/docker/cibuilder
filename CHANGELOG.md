# Changelog

## Unreleased

- No unreleased changes

## 1.6.1 (Released 2017-08-02)

- Fix: Fixed error with timezone in /init.sh

## 1.6.0 (Released 2018-07-30)

- Fix: Fixed known issue with Gitlab CI/CD where a build scripts exited with an error but the Gitlab job was still be flagged as successful
- Enhancement: Container initialization can be aborted if a service fails by using **EXIT_ON_SERVICE_FAILURE**
- Change: `/etc/default/...` files will no longer be read at startup
- Change: Services are now started at boot through the use of the **ENABLE_SERVICES** environment variable

## 1.5.0 (Released 2018-07-28)

- Feature: Added support for Gitlab Cloud, Community Edition and Enterprise Edition requiring the use of the new **ci_tool** setting
- Feature: **only_extra_tags** setting added to disable automatically adding default image tags
- Feature: Debugging can now be enabled by setting **CIBUILDER_ENABLE_DEBUG** variable to `true`
- Feature: Image caching can now be used through the **cache** settings in order to speed up builds
- Enhancement: Reasonable default values are now safely assumed for **settings.json** file
- Enhancement: Flags for `dockerd` command can now be overridden using **DOCKERD_ARGS** variable
- Enhancement: **automount**, **crond** and **syslog-ng** services are now available if needed
- Enhancement: `/opt/cibuilder/bin` folder has been added to the system path no longer requiring full paths to scripts in CI/CD config files
- Change: Build scripts moved to `/opt/cibuilder/bin` folder and renamed
- Note: `/usr/local/codebuild` path has been deprecated but will continue to work until version 2.0.0

## 1.0.2 (Released 2018-06-23)

- Fix: Fixed `docker build` to use `--file` flag for Dockerfile

## 1.0.1 (Released 2018-06-22)

- Enhancement: Added option to override `aws` command
- Enhancement: Added **glide** package

## 1.0.0 (Released 2018-06-04)

- Note: Initial release of the container image
- Note: Based on Docker in Docker Stable image
- Note: Installed dumb-init version 1.2.1
